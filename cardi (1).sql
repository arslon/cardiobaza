-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Май 31 2020 г., 17:17
-- Версия сервера: 10.4.11-MariaDB
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `cardi`
--

-- --------------------------------------------------------

--
-- Структура таблицы `doctors`
--

CREATE TABLE `doctors` (
  `id` int(11) NOT NULL,
  `fio` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `files`
--

CREATE TABLE `files` (
  `id` int(100) NOT NULL,
  `history_card_id` int(100) NOT NULL,
  `files_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `history_card`
--

CREATE TABLE `history_card` (
  `id` int(11) NOT NULL,
  `number_card` int(100) NOT NULL,
  `fio` varchar(255) NOT NULL,
  `birthday` varchar(255) NOT NULL,
  `place` varchar(255) NOT NULL,
  `critic` varchar(255) NOT NULL,
  `last_visit` varchar(255) NOT NULL,
  `files` varchar(255) NOT NULL,
  `doc_id` int(100) NOT NULL,
  `card` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `registr`
--

CREATE TABLE `registr` (
  `id` int(11) NOT NULL,
  `mode_reg` enum('1','2') NOT NULL COMMENT '1 - birlamchi.2- qayta',
  `special_number` int(100) NOT NULL,
  `fio` varchar(255) NOT NULL,
  `birthday` varchar(255) NOT NULL,
  `reg_date` varchar(255) NOT NULL,
  `time` varchar(100) NOT NULL,
  `place` text NOT NULL,
  `doc_id` int(100) NOT NULL,
  `history_card` enum('1','2','3') NOT NULL COMMENT '1- balnisada.2 - shaxsning o''zida'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 10,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `verification_token`) VALUES
(1, 'admin', 'DCwKPODEOLO9qOr5EuD2lBDtwRd_hPwz', '$2y$13$M7OqNBLJGiPjszzLYrn.E.DLfmOztiRALlcyRz68Qi8hL70LzanXm', NULL, 'murdavron@gmail.com', 10, 1584004382, 1585336824, 'XHpMZMYQnn67nplkw6MAYsHptci4Vm5P_1584004382'),
(3, 'doctor', 'DCwKPODEOLO9qOr5EuD2lBDtwRd_hPwz', '$2y$13$M7OqNBLJGiPjszzLYrn.E.DLfmOztiRALlcyRz68Qi8hL70LzanXm', NULL, 'doctor@mail.ru', 10, 1584004382, 1585336824, 'XHpMZMYQnn67nplkw6MAYsHptci4Vm5P_1584004382'),
(4, 'registrator', 'DCwKPODEOLO9qOr5EuD2lBDtwRd_hPwz', '$2y$13$M7OqNBLJGiPjszzLYrn.E.DLfmOztiRALlcyRz68Qi8hL70LzanXm', NULL, 'registrator@mail.ru', 10, 1584004382, 1585336824, 'XHpMZMYQnn67nplkw6MAYsHptci4Vm5P_1584004382');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `history_card_id` (`history_card_id`);

--
-- Индексы таблицы `history_card`
--
ALTER TABLE `history_card`
  ADD PRIMARY KEY (`id`),
  ADD KEY `doc_id` (`doc_id`);

--
-- Индексы таблицы `registr`
--
ALTER TABLE `registr`
  ADD PRIMARY KEY (`id`),
  ADD KEY `doc_id` (`doc_id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `doctors`
--
ALTER TABLE `doctors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `files`
--
ALTER TABLE `files`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT для таблицы `history_card`
--
ALTER TABLE `history_card`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `registr`
--
ALTER TABLE `registr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `files`
--
ALTER TABLE `files`
  ADD CONSTRAINT `files_ibfk_1` FOREIGN KEY (`history_card_id`) REFERENCES `history_card` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `history_card`
--
ALTER TABLE `history_card`
  ADD CONSTRAINT `history_card_ibfk_1` FOREIGN KEY (`doc_id`) REFERENCES `doctors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `registr`
--
ALTER TABLE `registr`
  ADD CONSTRAINT `registr_ibfk_1` FOREIGN KEY (`doc_id`) REFERENCES `doctors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
