<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\HistoryCardSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="history-card-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'number_card') ?>

    <?= $form->field($model, 'fio') ?>

    <?= $form->field($model, 'birthday') ?>

    <?= $form->field($model, 'place') ?>

    <?php // echo $form->field($model, 'critic') ?>

    <?php // echo $form->field($model, 'last_visit') ?>

    <?php // echo $form->field($model, 'files') ?>

    <?php // echo $form->field($model, 'doc_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
