<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\HistoryCard */

//$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'History Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="history-card-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php echo  Html::button('орқага',['id'=>'go-back','class'=>'btn btn-success ']) ?>
        <?= Html::a('янгилаш', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ўчириш', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'number_card',
            'fio',
            'birthday',
            'place',
            'critic',
            'last_visit',
            [
                'attribute' => 'card',
                'format' => 'raw',
                'visible' => true, // condition yozsa ham bo'ladi
                'value' => function ($model) {
                    if ($model->card == 1)
                    {
                        return '<span style="background-color: #00cc00" class="badge badge-warning">Регистратурада</span>';
                    }elseif ($model->card == 2)
                    {

                        return '<span style="background-color: red" class="badge badge-warning">Беморнинг ўзида</span>';
                    }elseif ($model->card == 3)
                    {
                        return  '<span style="background-color: #f39a0d" class="badge badge-warning">Кўрикда</span>';
                    }

                },
            ],
            'doc.fio',
        ],
    ]) ?>


</div>



<table class="table">
    <thead class="thead-light">
    <tr>
        Fayllar
        <br>

    </tr>
    </thead>
    <tbody>
    <tr>
<?php
$files  = \app\models\Files::find()->where(['history_card_id'=>$model])->all();
?>
<?php

foreach ($files as $count => $file):
?>

        <tr>
            <a href="<?='/'.$file->files_name?>" alt="files">
            <?=$model->fio.'_'. ++$count?>
        </tr>
    <br>
<?php
        endforeach;
        ?>

    </tr>

    </tbody>
</table>


<?php
$script= <<< JS
document.getElementById('go-back').addEventListener('click', () => {
  history.back();
});
JS;
$this->registerJs($script);
?>