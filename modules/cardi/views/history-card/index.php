<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\HistoryCardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="history-card-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::button('Карталарни рўйҳатга олиш', ['value'=>\yii\helpers\Url::to(['history-card/create']),'class' => 'btn btn-success','id'=>'modalButton']) ?>
    </p>


    <?php
    \yii\bootstrap\Modal::begin([
        'header' => 'Карталарни рўйҳатга олиш',
        'id' => 'modal',
        'size' => 'modal-lg',

    ]);
    echo "<div id='modalContent'></div>";
    \yii\bootstrap\Modal::end();

    ?>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => '#',
                'value' => function ($model, $key, $index, $grid) {
                    $pagination = $grid->grid->dataProvider->getPagination();
                    return $grid->grid->dataProvider->totalCount - $index - ($pagination !== false ? $pagination->getOffset() : 0);
                }
            ],
//            'id',
            'number_card',
            'fio',
            [
                'attribute' => 'birthday',
                'value' => 'birthday',

                'filter' => \yii\jui\DatePicker::widget([
                    'model'=>$searchModel,
                    'attribute'=>'birthday',
                    'language' => 'ru',
                    'dateFormat' => 'yyyy-MM-dd',
                ]),
                'format' => 'html',
            ],
            'place',
            'critic',
            [
                'attribute' => 'last_visit',
                'value' => 'last_visit',

                'filter' => \yii\jui\DatePicker::widget([
                    'model'=>$searchModel,
                    'attribute'=>'last_visit',
                    'language' => 'ru',
                    'dateFormat' => 'yyyy-MM-dd',
                ]),
                'format' => 'html',
            ],
//            'files',
            [
                'attribute' => 'doc_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return \app\models\Doctors::findOne(['id' => $model])->fio;

                },
            ],
            [
                'attribute' => 'card',
                'format' => 'raw',
                'visible' => true, // condition yozsa ham bo'ladi
                'value' => function ($model) {
                    if ($model->card == 1)
                    {
                        return '<span style="background-color: #00cc00" class="badge badge-warning">Регистратурада</span>';
                    }elseif ($model->card == 2)
                    {

                        return '<span style="background-color: red" class="badge badge-warning">Беморнинг ўзида</span>';
                    }elseif ($model->card == 3)
                    {
                        return  '<span style="background-color: #f39a0d" class="badge badge-warning">Кўрикда</span>';
                    }

                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {view}{delete}',

                'buttons' => [

                    'update' => function ($url, $model, $key) {

                        return Html::a('Yangilash', $url, ['class' => 'bg-blue label']);

                    },


                    'view' => function ($url, $model, $key) {

                        return Html::a('Ko\'rish<br>', $url, ['class' => 'bg-yellow label']);

                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], [
                            'class' => 'bg-red label',
                            'data' => [
                                'confirm' => 'Siz rostanham shu yozuvni o\'chirmoqchimisiz ?',
                                'method' => 'post',
                            ],
                        ]);
                    }
                ]
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>

<?php
$script= <<< JS

    $(function (){
        $('#modalButton').click(function(){
            $('#modal').modal('show')
            .find('#modalContent')
            .load($(this).attr('value'));
        });
    });
JS;
$this->registerJs($script);
?>