<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HistoryCard */

//$this->title = 'Create History Card';
$this->params['breadcrumbs'][] = ['label' => 'History Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="history-card-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
