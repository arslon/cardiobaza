<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\HistoryCard */
/* @var $form yii\widgets\ActiveForm */
?>
<?php echo  Html::button('орқага',['id'=>'go-back','class'=>'btn btn-success ']) ?>
<div class="history-card-form">
    <div class="row">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <div class="col-md-6">
            <?= $form->field($model, 'number_card')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'doc_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Doctors::find()->all(), 'id', 'fio'), ['prompt' => 'Doktorni tanlash'])->label('Do\'ktor') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'place')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'critic')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'files[]')->fileInput(['multiple' => true]) ?>

        </div>

        <div class="col-md-6">
            <div class="col-md-6">
                <?= $form->field($model, 'birthday')->widget(\yii\jui\DatePicker::className(), [
                    'language' => 'ru',
                    'dateFormat' => 'yyyy-MM-dd',
                ]) ?>

            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'last_visit')->widget(\yii\jui\DatePicker::className(), [
                    'language' => 'ru',
                    'dateFormat' => 'yyyy-MM-dd',
                ]) ?>

            </div>
        </div>



    <div class="col-md-6">
        <?= $form->field($model, 'card')->radioList(['1' => 'Регистратурада', '2' => 'Беморнинг ўзида','3'=>'Кўрикда']) ?>
    </div>
        <div class="col-md-12">
            <div class=" justify-content-end">
                <?= Html::submitButton('Сақлаш', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

    </div>




<?php
$script= <<< JS
document.getElementById('go-back').addEventListener('click', () => {
  history.back();
});
JS;
$this->registerJs($script);
?>