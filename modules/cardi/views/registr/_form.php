<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Registr */
/* @var $form yii\widgets\ActiveForm */
?>


    <div class="registr-form">

<?php echo Html::button('орқага', ['id' => 'go-back', 'class' => 'btn btn-success ']) ?>

<?php $form = ActiveForm::begin(); ?>
<?php
$username = Yii::$app->user->identity->username;
?>
    <div class="row">
        <br><br>
        <div class="col-md-6">
            <?= $form->field($model, 'mode_reg')->dropDownList([1 => 'birlamchi', 2 => 'qayta',], ['prompt' => '-Tanlash-', 'disabled' => $username !== 'doctor']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'special_number')->textInput($username == 'doctor' ? ['readonly' => false] : ['readonly' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'fio')->textInput($username == 'doctor' ? ['readonly' => false] : ['readonly' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'doc_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Doctors::find()->all(), 'id', 'fio'), ['prompt' => 'Doktorni tanlash', 'disabled' => $username !== 'doctor']) ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'place')->textarea($username == 'doctor' ? ['readonly' => false] : ['readonly' => true]) ?>
            <?= $form->field($model, 'reg_date')->hiddenInput(['value' => $model->checkAction($this->context->action->id)[1]])->label(false) ?>
            <?= $form->field($model, 'time')->hiddenInput(['value' => $model->checkAction($this->context->action->id)[0]])->label(false) ?>
        </div>


        <div class="col-md-6">
            <br>
            <br>
            <div class="col-md-6">
                <?php echo $form->field($model, 'birthday')->widget(\yii\jui\DatePicker::className(), [
                    'language' => 'ru',
                    'dateFormat' => 'yyyy-MM-dd',
                ]) ?>

            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'history_card')->radioList(['1' => 'Регистратурада', '2' => 'Беморнинг ўзида', '3' => 'Кўрикда']) ?>
            </div>


        </div>

    </div>


    <div class="form-group">
        <?= Html::submitButton('Сақлаш', ['class' => 'btn btn-success']) ?>

    </div>


<?php ActiveForm::end(); ?>

<?php
$script = <<< JS
document.getElementById('go-back').addEventListener('click', () => {
  history.back();
});
JS;
$this->registerJs($script);
?>