<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\NavBar;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Registr */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
    <div class="registr-index">

        <h1><?= Html::encode($this->title) ?></h1>



        <p>
            <?= Html::button('Рўйҳатга олиш', ['value'=>\yii\helpers\Url::to(['registr/create']),'class' => 'btn btn-success','id'=>'modalButton']) ?>
        </p>

        <?php
        \yii\bootstrap\Modal::begin([
            'header' => 'Ro\'yhatga olish',
            'id' => 'modal',
            'size' => 'modal-lg',

        ]);
        echo "<div id='modalContent'></div>";
        \yii\bootstrap\Modal::end();

        ?>

        <!--    --><?php //Pjax::begin(); ?>
        <!--    --><?php // echo $this->render('_search', ['model' => $searchModel]); ?>


        <?= GridView::widget([


            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [



//                ['class' => 'yii\grid\SerialColumn'],
//            'id',

                [
                    'label' => '#',
                    'value' => function ($model, $key, $index, $grid) {
                        $pagination = $grid->grid->dataProvider->getPagination();
                        return $grid->grid->dataProvider->totalCount - $index - ($pagination !== false ? $pagination->getOffset() : 0);
                    }
                ],
                [
                    'attribute' => 'mode_reg',
                    'format' => 'raw',
//                'visible' => true, // condition yozsa ham bo'ladi
                    'value' => function ($model) {
                        if ($model->mode_reg == 1)
                        {
                            return '<p style="color: green">Birlamchi</p>';
                        }elseif ($model->mode_reg == 2)
                        {

                            return '<p style="color: red">Qayta</p>';
                        }

                    },
                ],
                'special_number',
                'fio',
                [
                    'attribute' => 'birthday',
                    'value' => 'birthday',

                    'filter' => \yii\jui\DatePicker::widget([
                        'model'=>$searchModel,
                        'attribute'=>'birthday',
                        'language' => 'ru',
                        'dateFormat' => 'yyyy-MM-dd',
                    ]),
                    'format' => 'html',
                ],

                [
                    'attribute' => 'reg_date',
                    'value' => 'reg_date',

                    'filter' => \yii\jui\DatePicker::widget([
                        'model'=>$searchModel,
                        'attribute'=>'reg_date',
                        'language' => 'ru',
                        'dateFormat' => 'yyyy-MM-dd',
                    ]),
                    'format' => 'html',
                ],
                'time',

                'place:ntext',
                [
                    'attribute' => 'doc_id',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return \app\models\Doctors::findOne(['id' => $model])->fio;

                    },
                ],
                [
                    'attribute' => 'history_card',
                    'format' => 'raw',
                    'visible' => true, // condition yozsa ham bo'ladi
                    'value' => function ($model) {
                        if ($model->history_card == 1)
                        {
                            return '<span style="background-color: #00cc00" class="badge badge-warning">Регистратурада</span>';
                        }elseif ($model->history_card == 2)
                        {

                            return '<span style="background-color: red" class="badge badge-warning">Беморнинг ўзида</span>';
                        }elseif ($model->history_card == 3)
                        {
                            return  '<span style="background-color: #f39a0d" class="badge badge-warning">Кўрикда</span>';
                        }

                    },
                ],



                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {view}{delete}',

                    'buttons' => [

                        'update' => function ($url, $model, $key) {

                            return Html::a('Yangilash', $url, ['class' => 'bg-blue label']);

                        },


                        'view' => function ($url, $model, $key) {

                            return Html::a('Ko\'rish<br>', $url, ['class' => 'bg-yellow label']);

                        },
                        'delete' => function ($url, $model) {
                            return Yii::$app->user->identity->username =='doctor'? Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], [
                                'class' => 'bg-red label',
                                'data' => [
                                    'confirm' => 'Siz rostanham shu yozuvni o\'chirmoqchimisiz ?',
                                    'method' => 'post',
                                ],
                            ]):'';
                        }
                    ]
                ],
//            ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>

        <!--    --><?php //Pjax::end(); ?>

    </div>

<?php
$script= <<< JS

    $(function (){
        $('#modalButton').click(function(){
            $('#modal').modal('show')
            .find('#modalContent')
            .load($(this).attr('value'));
        });
    });
JS;
$this->registerJs($script);
?>