<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Registr */

//$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Registrs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="registr-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php echo  Html::button('орқага',['id'=>'go-back','class'=>'btn btn-success ']) ?>
        <?= Html::a('янгилаш ', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ўчириш', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'mode_reg',
            'special_number',
            'fio',
            'birthday',
            'reg_date',
            'place:ntext',
            [
                'attribute' => 'doc_id',
                'label' => 'Doktor',
                'value' => function ($model) {
                    return $model->doc->fio;
                }
            ],
//            'doc.fio',
            'time',
            [
                'attribute' => 'history_card',
                'format' => 'raw',
                'visible' => true, // condition yozsa ham bo'ladi
                'value' => function ($model) {
                    if ($model->history_card == 1)
                    {
                        return '<span style="background-color: #00cc00" class="badge badge-warning">Регистратурада</span>';
                    }elseif ($model->history_card == 2)
                    {

                        return '<span style="background-color: red" class="badge badge-warning">Беморнинг ўзида</span>';
                    }elseif ($model->history_card == 3)
                    {
                        return  '<span style="background-color: #f39a0d" class="badge badge-warning">Кўрикда</span>';
                    }

                },
            ],
        ],
    ]) ?>

</div>

<?php
$script= <<< JS
document.getElementById('go-back').addEventListener('click', () => {
  history.back();
});
JS;
$this->registerJs($script);
?>