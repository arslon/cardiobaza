<div class="row">
   <div class="container">
    <div class="col-lg-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?= count(\app\models\Registr::find()->all());  ?></h3>

                <p>Регистратура</p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
            <a href="<?=\yii\helpers\Url::to(['registr/index'])?>" class="small-box-footer">батафсил <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3><?= \app\models\HistoryCard::find()->count()?></h3>

                <p>Карталар</p>
            </div>
            <div class="icon">
                <i class="ion ion-stats-bars"></i>
            </div>
            <a href="<?= \yii\helpers\Url::to(['history-card/index'])?>" class="small-box-footer">батафсил <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3><?= \app\models\Doctors::find()->count() ?></h3>

                <p>Докторлар</p>
            </div>
            <div class="icon">
                <i class="ion ion-person-add"></i>
            </div>
            <a href="<?= \yii\helpers\Url::to(['doctors/index'])?>" class="small-box-footer">батафсил <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
   </div>
</div>