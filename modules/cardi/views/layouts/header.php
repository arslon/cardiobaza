<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <nav class="navbar navbar-static-top" role="navigation">

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/images/logo.png" class="user-image" alt="User Image"/>
                        <span class="hidden-xs">
                              <?php

                              if (Yii::$app->user->isGuest) {
                                  echo 'Mehmon';
                              } else {
                                  echo Yii::$app->user->identity->username;
                              }

                              ?>

                        </span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="/images/logo.png" class="img-circle"
                                 alt="User Image"/>

                            <p>
                                <?php

                                if (Yii::$app->user->isGuest) {
                                    echo 'Mehmon';
                                } else {
                                    echo Yii::$app->user->identity->username;
                                }

                                ?>
                            </p>
                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Sign out',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
