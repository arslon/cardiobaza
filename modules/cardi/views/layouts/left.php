<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/images/logo.png" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>
                    <?php
                    if (Yii::$app->user->isGuest)
                    {
                        echo 'Mehmon';
                    }else{
                        Yii::$app->user->identity->username;
                    }

                    ?>

                </p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Doctory', 'options' => ['class' => 'header']],
                    ['label' => 'Shifokor', 'icon' => 'user', 'url' => ['/cardi/doctors']],
                    ['label' => 'Ro\'yhatga olish', 'icon' => 'file-code-o', 'url' => ['/cardi/registr']],

                ],
            ]
        ) ?>

    </section>

</aside>
