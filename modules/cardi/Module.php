<?php

namespace app\modules\cardi;
use app\models\User;
use yii\web\ForbiddenHttpException;

/**
 * cardi module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\cardi\controllers';

    public $layout = 'main';
    public static $access_roles = [User::ROLE_ADMIN];
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }




}
