<div id="sticky-wrapper" class="sticky-wrapper" style="height: 75px;">

</div>


<section class="page-section">

    <div class="container relative">


        <h2 style="text-align: center" class="mb-70 mb-sm-40">
        <?=$pages->title?>
        </h2>

        <!-- Row -->
        <div class="row shadow" style="
    padding: 8px;
    background-color: #ddd;
    border-radius: 10px;
    border-color: #a72020;">

            <?=$pages->body?>

        </div>
        <!-- End Row -->

    </div>
</section>