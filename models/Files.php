<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "files".
 *
 * @property int $id
 * @property int $history_card_id
 * @property string $files_name
 *
 * @property HistoryCard $historyCard
 */
class Files extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['history_card_id', 'files_name'], 'required'],
            [['history_card_id'], 'integer'],
            [['files_name'], 'string', 'max' => 255],
            [['history_card_id'], 'exist', 'skipOnError' => true, 'targetClass' => HistoryCard::className(), 'targetAttribute' => ['history_card_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'history_card_id' => 'History Card ID',
            'files_name' => 'Files Name',
        ];
    }

    /**
     * Gets query for [[HistoryCard]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHistoryCard()
    {
        return $this->hasOne(HistoryCard::className(), ['id' => 'history_card_id']);
    }
}
