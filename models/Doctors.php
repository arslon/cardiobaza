<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "doctors".
 *
 * @property int $id
 * @property string $fio
 *
 * @property Registr[] $registrs
 */
class Doctors extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'doctors';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fio'], 'required'],
            [['fio'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'Doktor',
        ];
    }

    /**
     * Gets query for [[Registrs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegistrs()
    {
        return $this->hasMany(Registr::className(), ['doc_id' => 'id']);
    }
}
