<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "history_card".
 *
 * @property int $id
 * @property int $number_card
 * @property string $fio
 * @property string $birthday
 * @property string $place
 * @property string $critic
 * @property string $last_visit
 * @property string $files
 * @property int $doc_id
 * @property int $card
 *
// * @property Files[] $files
 * @property Doctors $doc
 */
class HistoryCard extends \yii\db\ActiveRecord
{
    public $files;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'history_card';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number_card', 'fio', 'birthday', 'place', 'critic', 'last_visit', 'files', 'doc_id','card'], 'required'],
            [['number_card', 'doc_id','card'], 'integer'],
            [['fio', 'birthday', 'place', 'critic', 'last_visit'], 'string', 'max' => 255],
            [['doc_id'], 'exist', 'skipOnError' => true, 'targetClass' => Doctors::className(), 'targetAttribute' => ['doc_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number_card' => 'Карта рақами',
            'fio' => 'Ф.И.О',
            'birthday' => 'Туғилган кун',
            'place' => 'Адресс',
            'critic' => 'Шикоят',
            'last_visit' => 'Охирги ташриф',
            'files' => 'Файл',
            'doc_id' => 'Доктор',
            'card' => '	Касаллик Тарихи',
        ];
    }

    /**
     * Gets query for [[Files0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(Files::className(), ['history_card_id' => 'id']);
    }

    /**
     * Gets query for [[Doc]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDoc()
    {
        return $this->hasOne(Doctors::className(), ['id' => 'doc_id']);
    }

    protected function fileUpload()
    {
        $files = UploadedFile::getInstances($this,'files');
        if ($files)
        {
            foreach ($files as $file)
            {
                $path = 'uploads/'.strtolower(str_replace(" ",'_',$this->fio));
                if (!file_exists($path))
                {
                    mkdir($path);
                }
                $dir = strtolower(str_replace(" ","_",$file->name)).uniqid().".".$file->extension;
                $file_path = "uploads/".strtolower(str_replace(" ",'_',$this->fio)).'/'.$dir;
                if ($file->saveAs($file_path))
                {
                    $filesModel = new Files();
                    $filesModel->history_card_id = $this->id;
                    $filesModel->files_name = $file_path;
                    $filesModel->save(false);
                }
            }
        }
    }





    public function afterSave($insert, $changedAttributes)
    {
        $this->fileUpload();
        return true;
    }

    public function beforeSave($insert)
    {
        return parent::beforeSave($insert);
    }
}
