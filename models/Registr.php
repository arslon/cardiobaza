<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "registr".
 *
 * @property int $id
 * @property string $mode_reg 1 - birlamchi.2- qayta
 * @property int $special_number
 * @property string $fio
 * @property string $birthday
 * @property string $reg_date
 * @property string $time
 * @property string $place
 * @property int $doc_id
 * @property string $history_card 1- balnisada.2 - shaxsning o'zida
 *
 * @property Doctors $doc
 */
class Registr extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'registr';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mode_reg', 'special_number', 'fio', 'birthday', 'reg_date', 'time', 'place', 'doc_id', 'history_card'], 'required'],
            [['mode_reg', 'place', 'history_card'], 'string'],
            [['special_number', 'doc_id'], 'integer'],
            [['fio', 'birthday', 'reg_date'], 'string', 'max' => 255],
            [['time'], 'string', 'max' => 100],
            [['doc_id'], 'exist', 'skipOnError' => true, 'targetClass' => Doctors::className(), 'targetAttribute' => ['doc_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mode_reg' => 'Қайта / Бирламчи',
            'special_number' => '№ йил бошидан',
            'fio' => 'Ф.и.о',
            'birthday' => 'Туғилган куни',
            'reg_date' => 'Регистратсиядан ўтган куни',
            'time' => 'Вақт',
            'place' => 'Адресс',
            'doc_id' => 'Доктор',
            'history_card' => 'Касаллик Тарихи',
        ];
    }

    /**
     * Gets query for [[Doc]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDoc()
    {
        return $this->hasOne(Doctors::className(), ['id' => 'doc_id']);
    }
    public function checkAction($action)
    {
        if ($action == 'update'){
            return [$this->time,$this->reg_date];
        }else{
            return [date('H:i'),date('Y-m-d')];
        }

    }
}
