<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Registr as RegistrModel;

/**
 * Registr represents the model behind the search form of `app\models\Registr`.
 */
class Registr extends RegistrModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'special_number', 'doc_id'], 'integer'],
            [['mode_reg', 'fio', 'birthday', 'reg_date','time', 'place', 'history_card'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RegistrModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'special_number' => $this->special_number,
            'doc_id' => $this->doc_id,
        ]);

        $query->andFilterWhere(['like', 'mode_reg', $this->mode_reg])
            ->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'birthday', $this->birthday])
            ->andFilterWhere(['like', 'reg_date', $this->reg_date])
            ->andFilterWhere(['like', 'time', $this->time])
            ->andFilterWhere(['like', 'place', $this->place])
            ->andFilterWhere(['like', 'history_card', $this->history_card]);

        return $dataProvider;
    }
}
