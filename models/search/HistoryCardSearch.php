<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\HistoryCard;

/**
 * HistoryCardSearch represents the model behind the search form of `app\models\HistoryCard`.
 */
class HistoryCardSearch extends HistoryCard
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'number_card',  'doc_id','card'], 'integer'],
            [['fio', 'birthday', 'place', 'critic', 'last_visit', 'files'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HistoryCard::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'number_card' => $this->number_card,
            'doc_id' => $this->doc_id,
        ]);

        $query->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'birthday', $this->birthday])
            ->andFilterWhere(['like', 'place', $this->place])
            ->andFilterWhere(['like', 'critic', $this->critic])
            ->andFilterWhere(['like', 'last_visit', $this->last_visit])
            ->andFilterWhere(['like', 'files', $this->files])
            ->andFilterWhere(['like', 'card', $this->card]);

        return $dataProvider;
    }
}
